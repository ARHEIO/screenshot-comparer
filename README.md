# Screenshot COmparer
## To Install

- Add the following line to the top of your `protractor.conf.js` file `const ProtractorScreenshots = require('./node_modules/screenshot-comparer/src/assets/protractor-screenshots.js');`
- The default onPrepare() function looks like this:

```
onPrepare() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
```

- Edit it so that it looks like this

```
onPrepare() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    jasmine.getEnv().addReporter(new ProtractorScreenshots());
  }
```
- This makes it so that protractor will take screenshots when a spec starts, and then when a spec completes, either successfully or unsuccessfully
- Run `./node_modules/screenshot-comparer/src/main.js new` to generate the needed config
- If you want to use the default, hit enter at the one question it asks
- Run `./node_modules/screenshot-comparer/src/main.js save` to generate the reference screenshots
- Set up the following npm run scripts

```
"vrt:save":"npm run e2e && ./node_modules/screenshot-comparer/src/main.js save"
```
```
"vrt:test":"npm run e2e && ./node_modules/screenshot-comparer/src/main.js test"
```
- When you checkout a branch, running the `vrt:save` script will generate your reference screenshots
- When you make your changes, running the `vrt:test` script will compare your changes to the previously generated reference screenshots
- You can also make `vrt:save` a post checkout git hook with the following:

```
touch .git/hooks/post-checkout
chmod u+x .git/hooks/post-checkout
```
- Then put the following content into the file

```
check_run package.json "npm run script vrt:save"
```