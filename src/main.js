#!/usr/bin/env node

// Requirements
const fs = require('fs');
const fse = require('fs-extra')
const path = require('path');
const PNG = require('pngjs').PNG;
const pixelmatch = require('pixelmatch');
const prompt = require('prompt');
const rimraf = require('rimraf');
const handlebars = require('handlebars');
const httpServer = require('http-server');


// Global Helpers
const beforeSlash = new RegExp(/(.*\/)/);
const afterSlash = new RegExp(/([^\/]+$)/);

// Global Variables
var args;
var refNamesToCompare;
var newNamesToCompare;

var newImages;
var imagesWithNoNewVersion;


var meerkatConfig;
const meerkatDir = './e2e/meerkat/';
const diffScreenshotDir = meerkatDir + 'dif-screenshots/';
const refScreenshotDir = meerkatDir + 'ref-screenshots/';
const meerkatConfigDir = meerkatDir + 'meerkatConfig.json';

var outputArray = [];

function main() {
  args = process.argv.slice(2);
  if (args[0] == 'new') {
    newMeerkat();
  }
  if (args[0] == 'save') {
    saveRef();
  }
  if (args[0] == 'test') {
    start();
  }
}

function newMeerkat() {
  // delete diff directory if it exists
  if (!fs.existsSync(meerkatDir)) {
    fs.mkdirSync(meerkatDir);
  };

  if (!fs.existsSync(diffScreenshotDir)) {
    fs.mkdirSync(diffScreenshotDir);
  }

  if (!fs.existsSync(refScreenshotDir)) {
    fs.mkdirSync(refScreenshotDir);
  }
  if (fs.existsSync(meerkatConfigDir)) {
    fs.unlinkSync(meerkatConfigDir);
  }

  prompt.start();

  prompt.get([{
      screenshots: '',
      description: 'where will your screenshots be generated? (include trailing slash)',
      required: false
    }], function (err, result) {
      var input = result.question;
      if (input.length > 0) {
        input = './e2e/screenshots'
      }
      var configObject =
        {
          "id": "meerkatVRT",
          "screenshots": input
        };
      fs.writeFileSync(meerkatConfigDir, JSON.stringify(configObject, null, 2), 'utf8');
  });
  
}

function saveRef() {
  if (fs.existsSync(meerkatConfigDir)) {
    meerkatConfig = fs.readFileSync(meerkatConfigDir, 'utf8');
  } else {
    console.log('No config file, run meerkat new');
    return;
  }

  meerkatConfig = JSON.parse(meerkatConfig);
  const newScreenshotsDir = meerkatConfig.screenshots;
  newNamesToCompare = getFiles(newScreenshotsDir);
  newNamesToCompare.forEach((filename) => {
    console.log(filename);
    fse.copy(filename, refScreenshotDir);
  })
}

function start() {
  if (fs.existsSync(meerkatConfigDir)) {
    meerkatConfig = fs.readFileSync(meerkatConfigDir, 'utf8');
  } else {
    console.log('No config file, run meerkat new');
    return;
  }

  meerkatConfig = JSON.parse(meerkatConfig);
  // Local variables
  const newScreenshotsDir = meerkatConfig.screenshots;
  const ROOT_DIR = process.cwd();

  // delete diff directory if it exists
  if (fs.existsSync(diffScreenshotDir)) {
    rimraf(diffScreenshotDir, () => {
      fs.mkdirSync(diffScreenshotDir);
    });
  } else {
    fs.mkdirSync(diffScreenshotDir);
  }

  if (fs.existsSync(meerkatDir + 'report/')) {
    rimraf(meerkatDir + 'report/', () => {
      fs.mkdirSync(meerkatDir + 'report/');
    });
  } else {
    fs.mkdirSync(meerkatDir + 'report/');
  }

  // Get filenames
  refNamesToCompare = getFiles(refScreenshotDir);
  newNamesToCompare = getFiles(newScreenshotsDir);

    // Comparisons of all the images that have something to compare
  var comparisonsMade = 0;
  var comparisonsToMake = refNamesToCompare.length
  if (comparisonsToMake === 0) {
    console.log('ERROR: There are no reference images');
  }
  refNamesToCompare.forEach(filename => {
    filename = slashRegex(filename, afterSlash);
    if (fs.existsSync(refScreenshotDir + filename) && !fs.existsSync(newScreenshotsDir + filename)) {
      console.log('ERROR:\n  The reference screenshot', refScreenshotDir + filename, 'does not have a comparison');
      comparisonsToMake -= 1;
      return;
    } else {
      var filesRead = 0;
      var refImage = fs.createReadStream(refScreenshotDir + filename).pipe(new PNG()).on('parsed', doneReading);
      var newImage = fs.createReadStream(newScreenshotsDir + filename).pipe(new PNG()).on('parsed', doneReading);
  
      function doneReading() {
        if (++filesRead < 2) return;
        console.log('Generating difference file for ', filename);
        var diff = new PNG({width: refImage.width, height: refImage.height});
        const difference = pixelmatch(refImage.data, newImage.data, diff.data, refImage.width, refImage.height, {threshold: 0.1});

        // MATHS
        var differencePercent = ((difference/(refImage.width * refImage.height)) * 100).toString().substring(0,4);
        // outputArray.push({imageName: filename, difference: differencePercent});
        outputArray.push({imageName: filename, "comparisonState": "BOTH", difference: differencePercent});

        // OUTPUT IMAGE
        var outputFilename = diffScreenshotDir +  filename.substring(0, filename.length - 3) + 'diff.png';
        diff.pack().pipe(fs.createWriteStream(outputFilename)).on('close', () => {
          // once we're finished, make up our final percentage array
          if (++comparisonsMade === comparisonsToMake - 1) {
            nowContinue();
          }
        });
      }
    }
  });
}

function nowContinue() {
  var tmpRefFilenames = [];
  var tmpNewFilenames = [];
  for (var i = 0; i < refNamesToCompare.length; i++) {
    tmpRefFilenames[i] = slashRegex(refNamesToCompare[i], afterSlash);
  }
  for (var i = 0; i < newNamesToCompare.length; i++) {
    tmpNewFilenames[i] = slashRegex(newNamesToCompare[i], afterSlash);
  }
  Array.prototype.diff = function(a) {
    return this.filter(function(i) {return a.indexOf(i) < 0;});
  };
  newImages = tmpNewFilenames.diff(tmpRefFilenames);
  imagesWithNoNewVersion = tmpRefFilenames.diff(tmpNewFilenames);
  newImages.forEach((filename) => {
    outputArray.push( {imageName: filename, "comparisonState": "NEW_IMAGE", difference: 0} );
  });
  imagesWithNoNewVersion.forEach((filename) => {
    outputArray.push( {imageName: filename, "comparisonState": "NO_COMPARISION", difference: 0} );
  })
  // console.log('The following are new images entirely:\n', newImages, '\nThe following images have no new versions:\n', imagesWithNoNewVersion);
  fs.writeFile(meerkatDir + 'meerkatDifferenceOutput.json', JSON.stringify(outputArray, null, 2), 'utf8', (err) => {
    if (err) throw err;
    console.log('\nSuccessfully compared images');
    
    console.log('\nThe following images have a difference of greater than 0%:');
    outputArray.forEach((element) => {
      if (element.difference > 0) {
        console.log(element.imageName);
      }
    });
    
    console.log('\nThe following images are new');
    outputArray.forEach((element) => {
      if (element.comparisonState === "NEW_IMAGE") {
        console.log(element.imageName);
      }
    });
    
    console.log('\nThe following reference images had nothing to compare against.\nMake sure that your tests have run correctly');
    outputArray.forEach((element) => {
      if (element.comparisonState === "NO_COMPARISON") {
        console.log(element.imageName);
      }
    });
    generateReport(outputArray);
  });
}

function generateReport(outputArray) {
  var uncompiledHitmonlee = 
  `
<!Doctype html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" href="./styles.css">
</head>
<body onclick="toggleModal(event);">
  <div class="page-top-banner"></div>
  <div class="global-container">\n`;
  outputArray.forEach((element) => {
    if (element.difference > 0) {
      uncompiledHitmonlee += '    <div class="diff-container" id="'+ element.imageName.slice(0, -4) + '.diff.png">\n';
      uncompiledHitmonlee += '      <img id="'+ element.imageName.slice(0, -4) + '.diff.png" src="../dif-screenshots/' + element.imageName.slice(0, -4) + '.diff.png"/>\n';
      uncompiledHitmonlee += '      <p id="'+ element.imageName.slice(0, -4) + '.diff.png">' + element.imageName.slice(0, -4).replace(/_/g, " ") + '</p>\n';
      uncompiledHitmonlee += '      <p id="'+ element.imageName.slice(0, -4) + '.diff.png" class="percentage">' + element.difference + '%</p>\n';
      uncompiledHitmonlee += '    </div>\n'
    }
  });

  uncompiledHitmonlee += `
  </div>
  <div id="modal">
    <img class="modal-image" id="modal-image" src="../dif-screenshots/should_override_an_article.done.diff.png"/>
    <button class="close-button" onclick="closeModal();">
      <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <g id="Cross" transform="translate(-5.000000, -5.000000)" fill="#8A939D">
                  <polygon id="cross" points="19 5.0001 12.706 11.2931 6.413 5.0001 5 6.4141 11.292 12.7081 5 19.0001 6.413 20.4141 12.706 14.1221 19 20.4141 20.414 19.0001 14.121 12.7081 20.414 6.4141"></polygon>
              </g>
          </g>
      </svg>
    </button>
  </div>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script type="text/javascript" src="./bundle.js"></script>
</body>
</html>`;


  
  fs.writeFile(meerkatDir + 'report/index.html', uncompiledHitmonlee, 'utf8', (err) => {
    console.log('\n\nOpen the file (', meerkatDir,'/report/index.html) to view the final report');
  });

  fse.copy(__dirname + '/assets/templates/styles.css', meerkatDir + 'report/styles.css');
  fse.copy(__dirname + '/assets/templates/bundle.js', meerkatDir + 'report/bundle.js');
  
}

function addToSortedArray(newObject, arr) {
  // an array of objects
  if (arr.length == 0) {
    arr.push(newObject);
  }
  for (var i = 0; i < arr.length; i++) {
    if (newObject.difference < arr[i].difference) {
        arr.splice(i, 0, newObject);
        break;
    }
  }
  return arr;
}

function getFiles (dir, files_){
  files_ = files_ || [];
  var files = fs.readdirSync(dir);
  for (var i in files){
      var name = dir[dir.length - 1] === '/' ? dir + files[i] : dir + '/' + files[i];
      if (fs.statSync(name).isDirectory()){
          getFiles(name, files_);
      } else {
          files_.push(name);
      }
  }
  return files_;
}

function slashRegex(inputString, regex) {
  var outputString;
  if (inputString[inputString.length - 1] === '/') {
    inputString = inputString.substring(0, inputString.length - 1);
  }
  outputString = regex.exec(inputString)[0]
  return outputString;
}

main();
