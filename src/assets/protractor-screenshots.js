/**
 * Written by Robin B (https://github.com/amindunited)
 */
const fs = require('fs');
const path = require('path');
const ROOT_DIR = process.cwd();

const targetDir = './e2e/screenshots/'

class ProtractorScreenshots {

  writeScreenShot(data, filename) {
    const fullPath = targetDir + filename;
    const dirPath = fullPath.substring(0, fullPath.lastIndexOf("/"));

    dirPath.split('/').reduce((parentDir, childDir) => {
      const curDir = path.resolve(parentDir, childDir);
      if (!fs.existsSync(curDir)) {
        fs.mkdirSync(curDir);
      }
      return curDir;
    });
    var stream = fs.createWriteStream(ROOT_DIR + fullPath);
    stream.write(new Buffer(data, 'base64'));
    stream.end();
  }

  jasmineStarted() {
    const dirToDelete = ROOT_DIR + targetDir;
    if (fs.existsSync(dirToDelete)) {
      fs.readdirSync(dirToDelete).forEach(function(file, index){
        var curPath = dirToDelete + "/" + file;
        if (fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(dirToDelete);
    }
  }

  specStarted(result) {
      browser.takeScreenshot().then((png) => {
        const filename = result.description.replace(/ /g, '_') + '.start.png';
        this.writeScreenShot(png, filename);
      });
  }

  specDone(result) {
    if (result.status === 'failed') {
      browser.takeScreenshot().then((png) => {
        const filename = result.description.replace(/ /g, '_') + '.failed.png';
        this.writeScreenShot(png, filename);
      });
    } else {
      browser.takeScreenshot().then((png) => {
        const filename = result.description.replace(/ /g, '_') + '.done.png';
        this.writeScreenShot(png, filename);
      });
    }
  }
}

module.exports = ProtractorScreenshots;
